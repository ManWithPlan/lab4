#include <stdio.h>
#include <limits.h>
#include <dirent.h>
#include <string.h>

int main(void){
	char pname[30];
	scanf("%s",pname);
	
    struct dirent *ent;
    FILE *f;
	DIR* dir = opendir("planets");
	
	char c;
	char name[1000];
	char lnp[40];
	while((ent = readdir(dir))!=NULL){
		memset(name,0,sizeof(name));
	    strcat(name,"planets\\");
	    strcat(name,ent->d_name);
		f = fopen(name,"r");
		if(f!=NULL){
			fgets(lnp,sizeof(lnp),f);
	    	if(strcmp(lnp,pname)==0) printf("%s\n",name);
			fclose(f);
		}
	}
	closedir(dir);
	return 0;
}
